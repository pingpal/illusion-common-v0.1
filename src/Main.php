<?php

namespace Illusion\Common;

use Illusion\Core\Launch;

use Illusion\Core\ConfigEdit;

use Illusion\Core\Util;

use Illusion\Config;

/**
 * # alias common
 * # access dev
 * # context cli
 */
class Main {

    protected $tables = [

        'user',
        'keys',
        'group',
        'mail',
        'nonce',
        'token',
        'session',
    ];

    function tables() {

        return $this->tables;
    }

    /**
     * # direct executer
     */
    static function executer($args, $in, $out, $ctx) {

        $out->end(PHP_BINARY);
    }

    /**
     * # direct launcher
     */
    static function launcher($args, $in, $out, $ctx) {

        global $argv;

        $out->end($argv[0]);
    }

    /**
     * # direct version
     */
    static function version($args, $in, $out, $ctx) {

        $out->end(Config::$VERSION);
    }

    /**
     * # direct cd
     */
    static function cd($args, $in, $out, $ctx) {

        $out->end(Config::$DOCROOT);
    }

    /**
     * # direct key
     */
    static function key($args, $in, $out, $ctx) {

        $out->end(Util::key());
    }

    /**
     * # direct sha
     */
    static function sha($args, $in, $out, $ctx) {

        $out->end(sha1(Util::key()));
    }

    /**
     * # direct mysql
     */
    static function mysql($args, $in, $out, $ctx) {

        if (@$args['new']) {

            $u = substr(Util::key(), 0, 5);
            $d = substr(Util::key(), 0, 5);
            $p = substr(Util::key(), 0, 9);

            $out->put('');
            $out->put("mysql -u root -p\nCREATE DATABASE $d;\nGRANT ALL ON $d.* TO '$u'@'localhost' IDENTIFIED BY '$p';");

            $out->put('');
            $out->put("Username: $u");
            $out->put("Password: $p");
            $out->put("Database: $d");

        } else if (@$args['login']) {

            $out->put('');
            $out->put('mysql -u' . Config::$MYSQL_USERNAME . ' -p' . Config::$MYSQL_PASSWORD . "\nuse " . Config::$MYSQL_DATABASE);

        } else {

            $out->put('');
            $out->put('Username: ' . Config::$MYSQL_USERNAME);
            $out->put('Password: ' . Config::$MYSQL_PASSWORD);
            $out->put('Database: ' . Config::$MYSQL_DATABASE);
        }
    }

    /**
    * # direct check
    */
    static function check($args, $in, $out, $ctx) {

        $ec = new EnvCheck($ctx, ConfigEdit::instance());

        $result = $ec->check([ 'version', 'posix', 'pcntl', 'cli', 'writable', 'mysqli', 'mysqlnd', 'mcrypt', 'database' ]);

        foreach ($result as $key => $val) {

            $out->put($key . ': ' . ($val['result'] ? '<32>OK</32>' : '<31>FAIL</31>'));

            if (!@$val['result']) {

                foreach (@$val['fix'] ?: [] as $row) {

                    $out->put($out->tty()->box(explode("\n", $row)));
                }
            }
        }

        $out->end();
    }

    /**
     * # direct setup
     */
    static function setup($args, $in, $out, $ctx) {

        $that = new self;

        $out->raw('setting up tables... ');

        foreach ($that->tables as $table) {

            sql()->createTableFromName($table);
        }

        $out->end('<green>ok!</green>');
    }

    /**
     * # direct teardown
     */
    static function teardown($args, $in, $out, $ctx) {

        $def = 'n';
        $ask = 'Are u really #$@&% sure?';
        $ans = strtolower($in->more("$ask " . ($def == 'y' ? '(YES|no)' : '(yes|NO)')));
        $ok = $def == 'y' ? $ans != 'n' && $ans != 'no' : $ans == 'y' || $ans == 'yes';

        if (!$ok) {

            $out->end('<yellow>Ok, safe travels.</yellow>');
        }

        $that = new self;

        $out->raw('tearing down tables... ');

        foreach (array_reverse($that->tables) as $table) {

            sql()->dropTableWithName($table);
        }

        $out->end('<green>ok!</green>');
    }

    /**
     * # direct init
     */
    static function init($args, $in, $out, $ctx) {

        $command = t($args, 'command/text');

        Util::isDirWritable(Config::$_PATH_WRITABLE) || Util::mkdir(Config::$_PATH_WRITABLE);

        $ce = ConfigEdit::instance();

        $entries = [ 'mysql-password', 'mysql-username', 'mysql-database' ];
        $default = $ce->getDefaultConfig('Illusion\Config', $entries);

        foreach ($default['entries'] as $name => $entry) {

            $config[$name] = $entry['value'];
        }

        if ($command) {

            $config['command'] = $command;
        }

        $config['executable'] = PHP_BINARY;
        $config['environments'] = 'access|dev|pc|pm|mail|webif';

        $config['mysql-username'] = substr($config['mysql-username'], 0, 5);
        $config['mysql-database'] = substr($config['mysql-database'], 0, 5);
        $config['mysql-password'] = substr($config['mysql-password'], 0, 9);

        $ce->setConfig('Illusion\Writable\Custom', $config);

        $out->end('<green>ok!</green>');
    }

    /**
     * # access public
     * # context cli
     * # direct debug
     */
    static function debug($args, $in, $out, $ctx) {

        $ce = ConfigEdit::instance();
        $config = $ce->getConfig('Illusion\Writable\Custom', null, null, true)['entries'];

        foreach ($config as $name => $entry) {

            $config[$name] = $entry['value'];
        }

        $config['debug'] = @$args[0] != 'off';

        $ce->setConfig('Illusion\Writable\Custom', $config);
        $out->end('turned debug ' . ($config['debug'] ? '<green>on</green>' : '<red>off</red>'));
    }

    /**
     * # access access
     * # direct access
     * # context api
     * # context cli
     */
    static function access($args, $in, $out, $ctx) {

        $launch = Launch::instance();
        $public = $launch->getAnnotatedCallables($ctx, false);
        unset($public[__METHOD__]);
        $callable = array_keys($public);
        $out->end($callable);
    }

    /**
     * # access public
     * # context cli
     * # direct env
     */
    static function env($args, $in, $out, $ctx) {

        $env = t($args, '1/text');

        $ce = ConfigEdit::instance();
        $config = $ce->getConfig('Illusion\Writable\Custom', null, null, true)['entries'];

        foreach ($config as $name => $entry) {

            $config[$name] = $entry['value'];
        }

        $envs = @$config['environments'] ? explode('|', $config['environments']) : [];

        $before = count($envs);

        if ($env && $args[0] == 'add') {

            if (!in_array($env, $envs)) {

                $envs[] = $env;
            }

        } else if ($env && $args[0] == 'del') {

            $envs = array_diff($envs, [ $env ]);

        } else {

            $out->end($envs ? 'environments: ' . implode(', ', $envs) : 'no environments');
        }

        $config['environments'] = implode('|', $envs);

        $ce->setConfig('Illusion\Writable\Custom', $config);

        $before == count($envs) ?
            $out->put('did nothing') :
            $out->put("$env " . ($args[0] == 'add' ? '<green>added</green>' : '<red>deleted</red>'));

        $out->end($envs ? 'environments: ' . implode(', ', $envs) : 'no environments');
    }
}
