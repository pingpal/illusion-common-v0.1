<?php

namespace Illusion\Common;

use Illusion\Core\Util;

use Illusion\Config;

use Exception;

/**
 * # alias envcheck
 */
class EnvCheck {

	protected $suite = [

		'version',
		'mysqli',
		'mysqlnd',
		'mcrypt',
		'cli',
		'posix',
		'pcntl',
		'writable',
		'reachable',
		'pathinfo',
		'executable',
		'database',
	];

	protected $ctx;

	function __construct($context = null) {

		$this->ctx = $context;
	}

	function check($suite = [], $exclude = false) {

		$result = [];

		foreach ($this->suite as $case) {

			if ($exclude xor in_array($case, $suite ?: $this->suite)) {

				$result[$case] = (array) Util::call([ $this, "test_$case" ]);
			}
		}

		return $result;
	}

	function test_writable() {

		$libRoot = Config::$LIBROOT;
		$wrtPath = Config::$_PATH_WRITABLE;

		$wrtPath = trim(str_replace($libRoot, '', $wrtPath), '/');

		$path = $libRoot . '/' . $wrtPath;

		return (object) [

			'result' => @is_dir($path) && @is_readable($path) && @is_writable($path),

			'fix' => [
				implode("\n", [
					'$ mkdir \'' . $path . '\'',
					'',
					'$ chmod 0775 \'' . $path . '\'',
				]),
			],
		];
	}

	function test_pathinfo() {

		$url = $this->ctx->vars()->current(null, null, 'pathinfo', 'test');

		return (object) [

			'url' => $url,
			'key' => 'response',
			'value' => 'ok',

			'fix' => [
				implode("\n", [
					'[NGINX]',
					'',
					'location / {',
					'  try_files $uri $uri/ /index.php$uri?$args;',
					'}',
					'',
					'location ~ \.php {',
					'  fastcgi_split_path_info ^(.+\.php)(/.+)$;',
					'',
					'  fastcgi_index index.php;',
					'  fastcgi_pass unix:/var/run/php5-fpm.sock;',
					'',
					'  fastcgi_param PATH_INFO $fastcgi_path_info;',
					'  fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;',
					'  fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;',
					'',
					'  include fastcgi_params;',
					'}',
				]),
				implode("\n", [
					'[APACHE]',
					'',
					'<IfModule mod_rewrite.c>',
					'  RewriteEngine On',
					'  RewriteBase /',
					'  RewriteRule ^index\.php$ - [L]',
					'  RewriteCond %{REQUEST_FILENAME} !-f',
					'  RewriteCond %{REQUEST_FILENAME} !-d',
					'  RewriteRule . /index.php [L]',
					'</IfModule>',
				]),
			],
		];
	}

	function test_reachable() {

		$docRoot = Config::$DOCROOT;
		$libRoot = Config::$LIBROOT;
		$wrtPath = Config::$_PATH_WRITABLE;

		$wrtPath = trim(str_replace($libRoot, '', $wrtPath), '/');

		$absPath = $libRoot . '/' . $wrtPath;
		$absFile = $absPath . '/' . 'test.txt';
		$relPath = str_replace($docRoot, '', $absPath);
		$relFile = $relPath . '/' . 'test.txt';

		$url = $this->ctx->vars()->home() . $relFile;

		$domain = $this->ctx->vars()->host() . $this->ctx->vars()->port(true);

		@file_put_contents($absFile, '{ "response" : "ok" }');

		return (object) [

			'url' => $url,
			'key' => 'response',
			'value' => 'ok',

			'fix' => [
				implode("\n", [
					'[NGINX]',
					'',
					'location ' . $relPath . ' {',
					'	deny all;',
					'}',
				]),
				implode("\n", [
					'[APACHE]',
					'',
					'<Directory ' . $relPath . '/>',
					'	Order Deny,allow',
					'	Deny from all',
					'</Directory>',
				]),
				implode("\n", [
					'[PHP Built-in web server]',
					'',
					'$ cd \'' . $docRoot . '\'',
					'',
					'$ echo \'<?php if (!preg_match("~^' . $relPath . '/.*~", $_SERVER["REQUEST_URI"])) return false;\' > router.php',
					'',
					'$ php -S ' . $domain . ' router.php',
				]),
			],
		];
	}

	function test_mysqli() {

		return (object) [

			'result' => function_exists('mysqli_connect'),

			'fix' => ['$ apt-get install php5-mysql'],
		];
	}

	function test_mysqlnd() {

		return (object) [

			'result' => function_exists('mysqli_stmt_get_result'),

			'fix' => ['$ apt-get install php5-mysqlnd'],
		];
	}

	function test_mcrypt() {

		return (object) [

			'result' => function_exists('mcrypt_create_iv'),

			'fix' => [
				implode("\n", [
					'$ apt-get install php5-mcrypt',
					'$ php5enmod mcrypt',
				]),
			],
		];
	}

	function test_version() {

		return (object) [

			'result' => version_compare(phpversion(), '5.5') >= 0,

			'fix' => [
				implode("\n", [
					'PHP 5.5 required. Current version is ' . phpversion(),
					'',
					'$ apt-get update',
					'$ apt-get upgrade',
					'$ apt-get install php5',
				]),
			],
		];
	}

	function test_posix() {

		return (object) [

			'result' => function_exists('posix_setsid'),
		];
	}

	function test_pcntl() {

		return (object) [

			'result' => function_exists('pcntl_fork'),
		];
	}

	function test_cli () {

		return (object) [

			'result' => strpos(`php -v`, 'PHP') === 0 && version_compare(explode(' ', `php -v`)[1], '5.4') >= 0,

			'fix' => [
				implode("\n", [
					'PHP-CLI required (min version 5.4)',
					'',
					'$ apt-get update',
					'$ apt-get upgrade',
					'$ apt-get install php5-cli',
				]),
			],
		];
	}

	function test_executable() {

		return (object) [

			'result' => is_executable(Config::$PATH_EXE),

			'fix' => ['Provde a path to the executable'],
		];
	}

	function test_database() {

		$ok = true;

		try {

			sql();

		} catch (Exception $e) {

			$ok = false;
		}

		return (object) [

			'result' => $ok,

			'fix' => [
				implode("\n", [
					'$ mysql -u root -p',
					'',
					'mysql> CREATE DATABASE ' . Config::$MYSQL_DATABASE. ';',
					'',
					"mysql> GRANT ALL ON " . Config::$MYSQL_DATABASE . ".* TO '" . Config::$MYSQL_USERNAME . "'@'localhost' IDENTIFIED BY '" . Config::$MYSQL_PASSWORD . "';",
				]),
			],
		];
	}
}
