<?php

namespace Illusion\Common\PM;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

use ZipArchive;
use DateTime;

class Update {

    static $LATEST = 'http://illusion.jonashall.in/latest.zip';

    static $INCLUDE = [

        'vendor/hallemaen/illusion',
        'vendor/hallemaen/illusion-common',
        'vendor/hallemaen/illusion-webif',
        'install.php',
    ];

    static $EXCLUDE_BRANCH = [

        'vendor/hallemaen/illusion/src/Writable',
    ];

    static $EXCLUDE_LEAF = [

        '.gitignore'
    ];

    static $SPEED = 0.002;
    static $SPINNER = [ '⊙⊙', '⊙⊙', '⊙⊙', '⊙⊙', '◡◡' ];
    static $LOADING = '  Please wait.. ';

    protected $t0;
    protected $progress;

    protected $root;
    protected $args;

    function __construct($root = null, $args = null) {

        $this->root = $root;
        $this->args = $args ?: [];
    }

    function begin() {

        $this->sayByeOnExit();
        echo $this->logo() . "\n";

        echo $a = "Welcome to IllUSION self update. \033[32m(press enter to carry out)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a);

        date_default_timezone_set('Europe/Stockholm');
        $folder = $this->dir();
        $this->registerProgress();
        $this->backup($folder);
        $temp = $this->generateRandomString();
        $temp = $folder . '/temp-' . $temp;
        $this->create($temp);
        $this->fetch($temp, self::$LATEST);
        $current = $this->getFileList($folder);
        $latest = $this->getFileList($temp);
        $remove = $this->diff($current, $latest);
        $this->removeFiles($folder, $remove);
        $this->copyOverwriteFiles($temp, $folder, $latest);
        $this->removeFolder($temp);
        $this->unregisterProgress();

        foreach (array_reverse(explode("\n", $this->logo())) as $a) $this->tipex($a);

        echo $a = "\n";

        $l[] = "\033[1mAll done!\033[0m";

        echo $this->box($l);
    }

    function dir() {

        return $this->root;
    }

    function cwd() {

        return getcwd();
    }

    function generateRandomString() {

        return bin2hex(openssl_random_pseudo_bytes(9));
    }

    function isMainFile() {

        return get_included_files()[0] == __FILE__;
    }

    function t() {

        return round((microtime(true) * 1000));
    }

    function reset() {

        $this->t0 = null;
    }

    function t0() {

        return $this->t0 ?: $this->t0 = $this->t();
    }

    function time() {

        $r = $this->t() - $this->t0();
        $t = floor($r * self::$SPEED);

        return max(0, $t);
    }

    function progress() {

        $tmpl = "\r%s\033[1m\033[36m%s\033[0m\033[0m\r";

        $loading = self::$LOADING;
        $spinner = self::$SPINNER[ $this->time() % count(self::$SPINNER) ];

        return sprintf($tmpl, $loading, $spinner);
    }

    function registerProgress() {

        $this->reset();

        $this->progress === null && register_tick_function(function () {

            echo $this->progress ? $this->progress() : null;
        });

        $this->progress = true;
    }

    function unregisterProgress() {

        $this->progress = false;

        echo str_repeat(' ', strlen($this->progress())) . "\r";
    }

    function logo() {

        $logo  = "        ___\n";
        $logo .= "       / / \\\n";
        $logo .= "      / /   \\\n";
        $logo .= "     / / / \ \\\n";
        $logo .= "    / / / \ \ \\\n";
        $logo .= "   / /_/___\ \ \\\n";
        $logo .= "  /_________\ \ \\\n";
        $logo .= "  \____________\/\n";
        $text  = "     IllUSION\n";

        $style  = "\033[1m\033[37m%s\033[0m\033[0m";
        $style .= "\033[1m\033[36m%s\033[0m\033[0m";

        return sprintf($style, $logo, $text);
    }

    function tipex($arg) {

        $args = func_num_args() == 1 ? is_array($arg) ? $arg : [ $arg ] : func_get_args();

        foreach (array_reverse($args) as $arg) {

            echo "\x1B[1A" . str_repeat(' ', strlen(preg_replace("/\033\[\d+m/", '', $arg))) . "\r";
        }
    }

    function readLine() {

        return defined('STDIN') && !feof(STDIN) ? substr(fgets(STDIN), 0, -1) : null;
    }

    function sayByeOnExit() {

        defined('SIGINT') && pcntl_signal(SIGINT, function () { die ("\rBye!\n"); }, 0);
    }

    function getString($text, $default = null, $validate = null) {

        $default && $text .= " \033[35m[default $default]\033[0m";
        $text .= " \033[33m(double tap ^C to abort)\033[0m";

        for ($i = 0; !$i || $error; $i++) {

            echo (@$error ?: $text) . "\n";
            $string = $this->readLine();

            $this->tipex($string);
            $i && $this->tipex($error);

            $string || $string = $default;
            $error = $validate ? $validate($string) : null;
            $error && $error = "\033[31m$error\033[0m";
        }

        $this->tipex($text);

        return $string;
    }

    function box($lines) {

        $rm = function ($s) { return preg_replace("/\033\[\d+m/", '', $s); };
        foreach ($lines as $line) $max = max(@$max, strlen($rm($line)));
        $box = $end = " \033[37m" . str_repeat('-', $max + 2) . "\033[0m\n";

        foreach ($lines as $line) {

            $box .= "\033[37m|\033[0m " . $line . str_repeat(' ', $max - strlen($rm($line))) . " \033[37m|\033[0m\n";
        }

        return $box . $end;
    }

    function exists($folder) {

        return @file_exists($folder);
    }

    function create($folder) {

        return @mkdir($folder);
    }

    function diff($current, $latest) {

        return array_values(array_diff($current, $latest));
    }

    function removeFiles($folder, $files) {

        for ($i = 0; $i < count($files); $i++) {

            $path = $folder . '/' . $files[$i];
            $dir = preg_replace('/[^\/]*$/', '', $path);

            @unlink($path);

            while (count(scandir($dir)) == 2) {

                @rmdir($dir);

                $dir = preg_replace('/[^\/]*$/', '', rtrim($dir, '/'));
            }
        }
    }

    function copyOverwriteFiles($from, $to, $files) {

        for ($i = 0; $i < count($files); $i++) {

            $path = $to . '/' . $files[$i];
            $dir = preg_replace('/[^\/]*$/', '', $path);

            if (!file_exists($dir)) {

                @mkdir($dir, 0777, true);
            }

            @copy($from . '/' . $files[$i], $path);
        }
    }

    function removeFolder($folder) {

        $files = array_diff(scandir($folder), [ '.', '..' ]);

        foreach ($files as $file) {

            $path = $folder . '/' . $file;
            is_dir($path) ? $this->removeFolder($path) : @unlink($path);
        }

        @rmdir($folder);
    }

    function fetch($folder, $url) {

        $zip = new ZipArchive();
        $temp = @tmpfile();
        $uri = @stream_get_meta_data($temp)['uri'];
        @file_put_contents($uri, @file_get_contents($url));
        @$zip->open($uri);
        @$zip->extractTo($folder);
        @$zip->close();
        @fclose($temp);
    }

    function backup($folder) {

        $date = DateTime::createFromFormat('U.u', microtime(true));
        $format = $date->format('Y-m-d.H:i:s.u');

        $path = $folder . '/backups';
        $name = $path . '/' . $format . '.zip';

        @is_dir($path) || @mkdir($path, 0777) | @chmod($path, 0777);

        @unlink($name);
        $zip = new ZipArchive;
        $zip->open($name, ZipArchive::CREATE);

        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder));

        foreach ($files as $file) {

            if ($file->isFile()) {

                $path = $file->getRealPath();
                $path = str_replace($folder . '/', '', $path);

                foreach ([ 'backups', 'temp' ] as $prefix) {

                    if (!strncmp($path, $prefix, strlen($prefix))) {

                        continue 2;
                    }
                }

                $zip->addFile($folder . '/' . $path, $path);
            }
        }

        $zip->close();
    }

    function getFileList($folder) {

        $array = [];

        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder));

        foreach ($files as $file) {

            if ($file->isFile()) {

                $path = $file->getRealPath();
                $path = str_replace($folder . '/', '', $path);

                foreach (self::$EXCLUDE_BRANCH as $prefix) {

                    if (!strncmp($path, $prefix, strlen($prefix))) {

                        continue 2;
                    }
                }

                foreach (self::$EXCLUDE_LEAF as $sufix) {

                    if (strtolower(substr($path, -strlen($sufix))) === strtolower($sufix)) {

                        continue 2;
                    }
                }

                foreach (self::$INCLUDE as $prefix) {

                    if (!strncmp($path, $prefix, strlen($prefix))) {

                        $array[] = $path;

                        break;
                    }
                }
            }
        }

        return $array;
    }
}
