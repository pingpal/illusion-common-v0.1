<?php

//@declare

namespace Illusion\Common\PM;

use ZipArchive;

//@init

class Install {

    static $LATEST = 'http://illusion.jonashall.in/latest.zip';
    static $LAUNCH = 'vendor/hallemaen/illusion/src/launch.php';

    static $SPEED = 0.002;
    static $SPINNER = [ '⊙⊙', '⊙⊙', '⊙⊙', '⊙⊙', '◡◡' ];
    static $LOADING = '  Please wait.. ';

    protected $t0;
    protected $progress;

    protected $root;
    protected $args;

    function __construct($root = null, $args = null) {

        $this->root = $root;
        $this->args = $args ?: [];
    }

    function begin() {

        $uninstall = @$this->args[0] == 'uninstall';
        $download = @$this->args[0] == 'download';
        $uninstall ? $this->uninstall() : $this->install($download);
    }

    function install($download = null) {

        $this->sayByeOnExit();
        echo $this->logo() . "\n";
        ($e = $this->check()) && $this->esc($e);

        echo $a = "Welcome to IllUSION installer. \033[32m(press enter to continue)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a);

        if ($download) {

            $folder = $this->getString('Enter install folder:', 'illusion', function ($folder) {

                if (!preg_match('/^[A-z0-9_.]+$/', $folder)) {

                    return '[A-z0-9_.] only';
                }

                if ($this->exists($this->cwd() . '/' . $folder)) {

                    return 'Folder must not exist.';
                }
            });

            $folder = $this->cwd() . '/' . $folder;

        } else {

            $folder = $this->dir();
        }

        $command = $this->getString('Enter command alias:', 'illusion', function ($string) {

            return preg_match('/^[A-z0-9_]+$/', $string) ? null : '[A-z0-9_] only';
        });

        $profile = $this->getString('Enter bash profile:', '.bash_profile', function ($string) {

            return preg_match('/^[A-z0-9_.]+$/', $string) ? null : '[A-z0-9_] only';
        });

        $profile = $this->home() . '/'. $profile;

        echo $a = "Folder: $folder\n";
        echo $b = "Command: $command\n";
        echo $c = "Profile: $profile\n";
        echo $d = "\n";
        echo $e = "Looks good? \033[32m(press enter to continue)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a, $b, $c, $d, $e);

        if ($download) {

            $this->registerProgress();
            $this->create($folder);
            $this->fetch($folder, self::$LATEST);
            $this->unregisterProgress();
        }

        $bin = $this->bin();
        $arg = $folder . '/' . self::$LAUNCH;

        $search = "/\n\n# IllUSION util\nalias $command=[^\n]+\n/";
        $string = "\n\n# IllUSION util\nalias $command='$bin $arg'\n";

        $this->insert($profile, $search, $string);

        foreach (array_reverse(explode("\n", $this->logo())) as $a) $this->tipex($a);

        echo $a = "\n";

        $l[] = "\033[1mAll done!\033[0m";
        $l[] = '';
        $l[] = "Logout and log back in, or run '\033[35m. $profile\033[0m'";
        $l[] = "to make the '\033[35m$command\033[0m' command available.";
        $l[] = '';
        $l[] = "Then run '\033[35m$command check\033[0m' to check your environment.";

        echo $this->box($l);

        `$bin $arg init --command=$command`;
    }

    function uninstall() {

        $this->sayByeOnExit();
        echo $this->logo() . "\n";

        echo $a = "You are about to uninstall IllUSION. \033[32m(press enter to continue)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a);

        $command = $this->getString('Enter command alias:', 'illusion', function ($string) {

            return preg_match('/^[A-z0-9_]+$/', $string) ? null : '[A-z0-9_] only';
        });

        $profile = $this->getString('Enter bash profile:', '.bash_profile', function ($string) {

            return preg_match('/^[A-z0-9_.]+$/', $string) ? null : '[A-z0-9_] only';
        });

        $profile = $this->home() . '/'. $profile;

        echo $a = "Command: $command\n";
        echo $b = "Profile: $profile\n";
        echo $c = "\n";
        echo $d = "Looks good? \033[32m(press enter to continue)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a, $b, $c, $d);

        $search = "/\n\n# IllUSION util\nalias $command=[^\n]+\n/";

        $this->strip($profile, $search);

        foreach (array_reverse(explode("\n", $this->logo())) as $a) $this->tipex($a);

        echo $a = "\n";

        $l[] = "\033[1mUninstall complete!\033[0m";
        $l[] = '';
        $l[] = 'Logout and log back in to have the uninstall take effect.';

        echo $this->box($l);
    }

    function check() {

        $suite = [

            [ 'run from cli',           php_sapi_name() == 'cli'                    ],
            [ 'version 5.4 required',   version_compare(phpversion(), '5.4') >= 0   ],
            [ 'no stdin',               defined('STDIN')                            ],
            [ 'unkown bin',             defined('PHP_BINARY')                       ],
            [ 'no ziparchive ext',      class_exists('ZipArchive')                  ],
        ];

        foreach ($suite as $test) if (!$test[1]) return $test[0];
    }

    function out($arg) {

        echo $arg;
    }

    function red($arg) {

        return "\033[31m$arg\033[0m";
    }

    function esc($arg) {

        die ($this->red($arg) . "\n");
    }

    function bin() {

        return PHP_BINARY;
    }

    function dir() {

        return $this->root;
    }

    function cwd() {

        return getcwd();
    }

    function home() {

        return rtrim($_SERVER['HOME'], '/');
    }

    function isMainFile() {

        return !get_included_files() || get_included_files()[0] == __FILE__;
    }

    function t() {

        return round((microtime(true) * 1000));
    }

    function reset() {

        $this->t0 = null;
    }

    function t0() {

        return $this->t0 ?: $this->t0 = $this->t();
    }

    function time() {

        $r = $this->t() - $this->t0();
        $t = floor($r * self::$SPEED);

        return max(0, $t);
    }

    function progress() {

        $tmpl = "\r%s\033[1m\033[36m%s\033[0m\033[0m\r";

        $loading = self::$LOADING;
        $spinner = self::$SPINNER[ $this->time() % count(self::$SPINNER) ];

        return sprintf($tmpl, $loading, $spinner);
    }

    function registerProgress() {

        $this->reset();

        $this->progress === null && register_tick_function(function () {

            echo $this->progress ? $this->progress() : null;
        });

        $this->progress = true;
    }

    function unregisterProgress() {

        $this->progress = false;

        echo str_repeat(' ', strlen($this->progress())) . "\r";
    }

    function logo() {

        $logo  = "        ___\n";
        $logo .= "       / / \\\n";
        $logo .= "      / /   \\\n";
        $logo .= "     / / / \ \\\n";
        $logo .= "    / / / \ \ \\\n";
        $logo .= "   / /_/___\ \ \\\n";
        $logo .= "  /_________\ \ \\\n";
        $logo .= "  \____________\/\n";
        $text  = "     IllUSION\n";

        $style  = "\033[1m\033[37m%s\033[0m\033[0m";
        $style .= "\033[1m\033[36m%s\033[0m\033[0m";

        return sprintf($style, $logo, $text);
    }

    function tipex($arg) {

        $args = func_num_args() == 1 ? is_array($arg) ? $arg : [ $arg ] : func_get_args();

        foreach (array_reverse($args) as $arg) {

            echo "\x1B[1A" . str_repeat(' ', strlen(preg_replace("/\033\[\d+m/", '', $arg))) . "\r";
        }
    }

    function readLine() {

        return defined('STDIN') && !feof(STDIN) ? substr(fgets(STDIN), 0, -1) : null;
    }

    function sayByeOnExit() {

        defined('SIGINT') && pcntl_signal(SIGINT, function () { die ("\rBye!\n"); }, 0);
    }

    function getString($text, $default = null, $validate = null) {

        $default && $text .= " \033[35m[default $default]\033[0m";
        $text .= " \033[33m(double tap ^C to abort)\033[0m";

        for ($i = 0; !$i || $error; $i++) {

            echo (@$error ?: $text) . "\n";
            $string = $this->readLine();

            $this->tipex($string);
            $i && $this->tipex($error);

            $string || $string = $default;
            $error = $validate ? $validate($string) : null;
            $error && $error = "\033[31m$error\033[0m";
        }

        $this->tipex($text);

        return $string;
    }

    function box($lines) {

        $rm = function ($s) { return preg_replace("/\033\[\d+m/", '', $s); };
        foreach ($lines as $line) $max = max(@$max, strlen($rm($line)));
        $box = $end = " \033[37m" . str_repeat('-', $max + 2) . "\033[0m\n";

        foreach ($lines as $line) {

            $box .= "\033[37m|\033[0m " . $line . str_repeat(' ', $max - strlen($rm($line))) . " \033[37m|\033[0m\n";
        }

        return $box . $end;
    }

    function strip($file, $search) {

        $this->replace($file, $search, null);
    }

    function replace($file, $search, $replace) {

        $string = @file_get_contents($file);
        $ok = $string != $replaced = preg_replace($search, $replace, $string);
        $ok && @fwrite($h = @fopen($file, 'w'), $replaced) | @fclose($h);
    }

    function append($file, $search, $append) {

        $string = @file_get_contents($file);
        $ok = !preg_match($search, $string);
        $ok && @fwrite($h = @fopen($file, 'a'), $append) | @fclose($h);
    }

    function insert($file, $search, $string) {

        $this->strip($file, $search, $string);
        $this->append($file, $search, $string);
    }

    function exists($folder) {

        return @file_exists($folder);
    }

    function create($folder) {

        return @mkdir($folder);
    }

    function fetch($folder, $url) {

        $zip = new ZipArchive();
        $temp = @tmpfile();
        $uri = @stream_get_meta_data($temp)['uri'];
        @file_put_contents($uri, @file_get_contents($url));
        @$zip->open($uri);
        @$zip->extractTo($folder);
        @$zip->close();
        @fclose($temp);
    }
}
