<?php

namespace Illusion\Common\PM;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

use ZipArchive;

class Deploy {

    static $SITES = '/usr/share/nginx';

    static $ARCHIVE = 'illusion.zip';
    static $INSTALL = 'vendor/hallemaen/illusion-common/src/PM/Install.php';

    static $LATEST = 'latest.zip';
    static $GET = 'get/index.php';

    static $INCLUDE = [

        'vendor/autoload.php',
        'vendor/composer',
        'vendor/evenement',
        'vendor/guzzle',
        'vendor/hallemaen/illusion',
        'vendor/hallemaen/illusion-common',
        'vendor/hallemaen/illusion-webif',
        'vendor/psr',
        'vendor/react',
        'vendor/swiftmailer',
        'composer.phar',
        'composer.json',
        'illusion.json',
        'install.php',
    ];

    static $EXCLUDE_BRANCH = [

        'vendor/hallemaen/illusion/src/Writable'
    ];

    static $EXCLUDE_LEAF = [

        '.gitignore'
    ];

    static $SPEED = 0.002;
    static $SPINNER = [ '⊙⊙', '⊙⊙', '⊙⊙', '⊙⊙', '◡◡' ];
    static $LOADING = '  Please wait.. ';

    protected $t0;
    protected $progress;

    protected $root;
    protected $args;

    function __construct($root = null, $args = null) {

        $this->root = $root;
        $this->args = $args ?: [];
    }

    function begin() {

        $this->sayByeOnExit();
        echo $this->logo() . "\n";

        $args = implode(' ', $this->args);
        $args || $this->esc('Bad arguments');

        $ok = preg_match('/^(.+) ([^ ]+)$/', $args, $match);
        $ok || $this->esc('Bad arguments');

        echo $a = "Welcome to IllUSION deploy. \033[32m(press enter to continue)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a);

        list(, $login, $server) = $match ?: [0, 0, 0];
        $site = $this->extractSiteName($server);

        $sites = $this->getString('Enter sites folder:', self::$SITES, function ($string) {

            return preg_match('/^[A-z0-9\/_-]+$/', $string) ? null : '[A-z0-9/_-] only';
        });

        $site = $this->getString('Enter site name', $site, function ($string) {

            return preg_match('/^[A-z0-9_.-]+$/', $string) ? null : '[A-z0-9_-] only';
        });

        $latest = $this->getString('Enter latest zip', self::$LATEST, function ($string) {

            return preg_match('/^[A-z0-9\/_.-]+$/', $string) ? null : '[A-z0-9/_.-] only';
        });

        $get = $this->getString('Enter get index', self::$GET, function ($string) {

            return preg_match('/^[A-z0-9\/_.-]+$/', $string) ? null : '[A-z0-9/_.-] only';
        });

        $latest = $this->buildLatestPath($sites, $site, $latest);
        $get = $this->buildGetPath($sites, $site, $get);

        echo $a = "\n";
        echo $b = "Latest path: $latest\n";
        echo $c = "Get index path: $get\n";
        echo $d = "\n";
        echo $e = "Looks good? \033[32m(press enter to continue)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a, $b, $c, $d, $e);

        $folder = $this->dir();
        $this->registerProgress();
        $stopover = $this->generateRandomString();
        $this->createZipArchive($folder, self::$ARCHIVE);
        $this->performSecureCopy($login, $server, $folder, self::$ARCHIVE, $latest);
        $this->removeFile($folder, self::$ARCHIVE);
        $this->prepareInstallFile($folder, self::$INSTALL, $stopover);
        $this->performSecureCopy($login, $server, $folder, $stopover, $get);
        $this->removeFile($folder, $stopover);
        $this->unregisterProgress();

        foreach (array_reverse(explode("\n", $this->logo())) as $a) $this->tipex($a);

        echo $a = "\n";

        $l[] = "\033[1mAll done!\033[0m";

        echo $this->box($l);
    }

    function out($arg) {

        echo $arg;
    }

    function red($arg) {

        return "\033[31m$arg\033[0m";
    }

    function esc($arg) {

        die($this->red($arg) . "\n");
    }

    function dir() {

        return $this->root;
    }

    function cwd() {

        return getcwd();
    }

    function generateRandomString() {

        return bin2hex(openssl_random_pseudo_bytes(9));
    }

    function isMainFile() {

        return get_included_files()[0] == __FILE__;
    }

    function t() {

        return round((microtime(true) * 1000));
    }

    function reset() {

        $this->t0 = null;
    }

    function t0() {

        return $this->t0 ?: $this->t0 = $this->t();
    }

    function time() {

        $r = $this->t() - $this->t0();
        $t = floor($r * self::$SPEED);

        return max(0, $t);
    }

    function progress() {

        $tmpl = "\r%s\033[1m\033[36m%s\033[0m\033[0m\r";

        $loading = self::$LOADING;
        $spinner = self::$SPINNER[ $this->time() % count(self::$SPINNER) ];

        return sprintf($tmpl, $loading, $spinner);
    }

    function registerProgress() {

        $this->reset();

        $this->progress === null && register_tick_function(function () {

            echo $this->progress ? $this->progress() : null;
        });

        $this->progress = true;
    }

    function unregisterProgress() {

        $this->progress = false;

        echo str_repeat(' ', strlen($this->progress())) . "\r";
    }

    function logo() {

        $logo  = "        ___\n";
        $logo .= "       / / \\\n";
        $logo .= "      / /   \\\n";
        $logo .= "     / / / \ \\\n";
        $logo .= "    / / / \ \ \\\n";
        $logo .= "   / /_/___\ \ \\\n";
        $logo .= "  /_________\ \ \\\n";
        $logo .= "  \____________\/\n";
        $text  = "     IllUSION\n";

        $style  = "\033[1m\033[37m%s\033[0m\033[0m";
        $style .= "\033[1m\033[36m%s\033[0m\033[0m";

        return sprintf($style, $logo, $text);
    }

    function tipex($arg) {

        $args = func_num_args() == 1 ? is_array($arg) ? $arg : [ $arg ] : func_get_args();

        foreach (array_reverse($args) as $arg) {

            echo "\x1B[1A" . str_repeat(' ', strlen(preg_replace("/\033\[\d+m/", '', $arg))) . "\r";
        }
    }

    function readLine() {

        return defined('STDIN') && !feof(STDIN) ? substr(fgets(STDIN), 0, -1) : null;
    }

    function sayByeOnExit() {

        defined('SIGINT') && pcntl_signal(SIGINT, function () { die("\rBye!\n"); }, 0);
    }

    function getString($text, $default = null, $validate = null) {

        $default && $text .= " \033[35m[default $default]\033[0m";
        $text .= " \033[33m(double tap ^C to abort)\033[0m";

        for ($i = 0; !$i || $error; $i++) {

            echo (@$error ?: $text) . "\n";
            $string = $this->readLine();

            $this->tipex($string);
            $i && $this->tipex($error);

            $string || $string = $default;
            $error = $validate ? $validate($string) : null;
            $error && $error = "\033[31m$error\033[0m";
        }

        $this->tipex($text);

        return $string;
    }

    function box($lines) {

        $rm = function ($s) { return preg_replace("/\033\[\d+m/", '', $s); };
        foreach ($lines as $line) $max = max(@$max, strlen($rm($line)));
        $box = $end = " \033[37m" . str_repeat('-', $max + 2) . "\033[0m\n";

        foreach ($lines as $line) {

            $box .= "\033[37m|\033[0m " . $line . str_repeat(' ', $max - strlen($rm($line))) . " \033[37m|\033[0m\n";
        }

        return $box . $end;
    }

    function extractSiteName($server) {

        return array_reverse(explode('@', $server))[0];
    }

    function buildLatestPath($sites, $site, $latest) {

        return '/' . trim($sites, '/') . '/' . $site . '/' . $latest;
    }

    function buildGetPath($sites, $site, $get) {

        return '/' . trim($sites, '/') . '/' . $site .'/' . $get;
    }

    function executeRemoteScript($login, $server, $script) {

        return `ssh $login $server $script`;
    }

    function performSecureCopy($login, $server, $folder, $source, $destination) {

        return `scp $login $folder/$source $server:$destination`;
    }

    function prepareInstallFile($folder, $install, $stopover) {

        $content = @file_get_contents($folder . '/' . $install);

        $content = str_replace('<?php' , '', $content);
        $content = str_replace('//@declare' , 'declare(ticks = 1);', $content);
        $content = str_replace('//@init', "(new Install(null, [ 'download' ]))->begin();", $content);

        @file_put_contents($folder . '/' . $stopover, $content);
    }

    function removeFile($folder, $file) {

        @unlink($folder . '/' . $file);
    }

    function createZipArchive($folder, $name) {

        $name = $folder . '/' . $name;

        @unlink($name);
        $zip = new ZipArchive;
        $zip->open($name, ZipArchive::CREATE);

        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder));

        foreach ($files as $file) {

            if ($file->isFile()) {

                $path = $file->getRealPath();
                $path = str_replace($folder . '/', '', $path);

                foreach (self::$EXCLUDE_BRANCH as $prefix) {

                    if (!strncmp($path, $prefix, strlen($prefix))) {

                        continue 2;
                    }
                }

                foreach (self::$EXCLUDE_LEAF as $sufix) {

                    if (strtolower(substr($path, -strlen($sufix))) === strtolower($sufix)) {

                        continue 2;
                    }
                }

                foreach (self::$INCLUDE as $prefix) {

                    if (!strncmp($path, $prefix, strlen($prefix))) {

                        $zip->addFile($folder . '/' . $path, $path);

                        break;
                    }
                }
            }
        }

        $zip->close();
    }
}
