<?php

// add ,"$text\n" to others (in getString) (Install/Deploy/Update)

namespace Illusion\Common\PM;

use Illusion\Config;

use RecursiveDirectoryIterator;

use RecursiveIteratorIterator;

use Exception;

class Setup {

    static $SPEED = 0.002;
    static $SPINNER = [ '⊙⊙', '⊙⊙', '⊙⊙', '⊙⊙', '◡◡' ];
    static $LOADING = '  Please wait.. ';

    protected $t0;
    protected $progress;

    protected $context;

    function __construct($context) {

        $this->context = $context;
    }

    function unSetup() {
        // clean jsons
    }

    function setupApp() {

    }

    function setupProject() {

        $this->sayByeOnExit();
        echo $this->logo() . "\n";

        echo $a = "Welcome to IllUSION project setup. \033[32m(press enter to continue)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a);

        $remote = $this->getString('Enter remote ssh git url:', null, function ($string, $text) {

            if (!preg_match('/^[A-z0-9\/_.:@-]+$/', $string)) {

                return 'Does not appear to be a git repository';
            }

            $this->tipex($text);
            $this->registerProgress();

            exec("git ls-remote $string 2>&1", $output, $error);

            $output = implode("\n", $output);

            $this->unregisterProgress();
            echo $text;

            if ($error) {

                return 'Could not read from remote repository';
            }

            if (preg_match('/\S/', $output)) {

                return 'Remote repository appears to be non empty';
            }
        });

        for (;;) {

            $vendor = $this->getString('Enter vendor name:', null, function ($string) {

                return preg_match('/^[A-z0-9_-]+$/', $string) ? null : '[A-z0-9_-] only';
            });

            $project = $this->getString('Enter project name:', null, function ($string) {

                return preg_match('/^[A-z0-9_-]+$/', $string) ? null : '[A-z0-9_-] only';
            });

            $vendor = strtolower($vendor);
            $project = strtolower($project);

            $folder = Config::$DOCROOT . "/vendor/$vendor/$project";

            if (file_exists($folder)) {

                echo $a = "Folder \033[35mvendor/$vendor/$project\033[0m must not exist. \033[32m(press enter to try again)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

                $this->tipex($this->readLine());
                $this->tipex($a);

            } else {

                break;
            }
        }

        $namespace = str_replace(' ', '', ucwords(preg_replace('/[_-]/', ' ', $vendor)) . '\\' . ucwords(preg_replace('/[_-]/', ' ', $project)));

        $namespace = $this->getString('Enter topmost unique namespace:', $namespace, function ($string) {

            return preg_match('/^([A-Z][A-z]+\\\\?)+$/', $string) ? null : 'Like this: Name[\Space ...]';
        });

        $namespace = rtrim($namespace, '\\');

        echo $a = "Remote: $remote\n";
        echo $b = "Vendor: $vendor\n";
        echo $c = "Project: $project\n";
        echo $d = "Namespace: $namespace\n";
        echo $e = "Folder: $folder\n";
        echo $f = "\n";
        echo $g = "Looks good? \033[32m(press enter to continue)\033[0m \033[33m(double tap ^C to abort)\033[0m\n";

        $this->tipex($this->readLine());
        $this->tipex($a, $b, $c, $d, $e, $f, $g);

        if (!file_exists(Config::$DOCROOT . "/vendor/$vendor")) {

            mkdir(Config::$DOCROOT . "/vendor/$vendor");
        }

        mkdir($folder);
        mkdir("$folder/src");

        file_put_contents("$folder/README.md", $this->genReadme($project, $remote));
        file_put_contents("$folder/composer.json", $this->genComposer($vendor, $project, $namespace));
        file_put_contents("$folder/src/Main.php", $this->genMain($namespace, $project, $project));

        passthru("( cd $folder ; git init ; git add . ; git commit -m initial ; git remote add origin $remote ; git push -u origin master )", $error);

        if ($error) {

            throw new Exception("get setup went wrong");
        }

        $rdiri = new RecursiveDirectoryIterator($folder, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($rdiri, RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($files as $info) {

            $path = $info->getRealPath();

            if ($info->isDir()) {

                rmdir($path);

            } else {

                unlink($path);
            }
        }

        rmdir($folder);

        PM::add([ $remote ], $this->context->in(), $this->context->out(), $this->context);

        $this->context->out()->put('');

        $_[] = '<bold>All done!</bold>';
        $_[] = '';
        $_[] = "Location: $folder";
        $_[] = '';
        $_[] = "Run '<magenta>" . Config::$COMMAND . " $project.main --name=\"$vendor\"</magenta>' to try out your new app!";

        $this->context->out()->end($this->context->out()->tty()->box($_));
    }

    function genReadme($name, $remote) {

        return sprintf(Statik::$README, $name, $remote);
    }

    function genComposer($vendor, $project, $namespace) {

        $namespace = str_replace('\\', '\\\\', $namespace);

        return sprintf(Statik::$COMPOSER, $vendor, $project, $vendor, $project, $vendor, $project, $namespace);
    }

    function genMain($namespace, $alias, $access) {

        return sprintf(Statik::$MAIN, $namespace, $alias, $access);
    }

    function t() {

        return round((microtime(true) * 1000));
    }

    function reset() {

        $this->t0 = null;
    }

    function t0() {

        return $this->t0 ?: $this->t0 = $this->t();
    }

    function time() {

        $r = $this->t() - $this->t0();
        $t = floor($r * self::$SPEED);

        return max(0, $t);
    }

    function progress() {

        $tmpl = "\r%s\033[1m\033[36m%s\033[0m\033[0m\r";

        $loading = self::$LOADING;
        $spinner = self::$SPINNER[ $this->time() % count(self::$SPINNER) ];

        return sprintf($tmpl, $loading, $spinner);
    }

    function registerProgress() {

        $this->reset();

        $this->progress === null && register_tick_function(function () {

            echo $this->progress ? $this->progress() : null;
        });

        $this->progress = true;
    }

    function unregisterProgress() {

        $this->progress = false;

        echo str_repeat(' ', strlen($this->progress())) . "\r";
    }

    function logo() {

        $logo  = "        ___\n";
        $logo .= "       / / \\\n";
        $logo .= "      / /   \\\n";
        $logo .= "     / / / \ \\\n";
        $logo .= "    / / / \ \ \\\n";
        $logo .= "   / /_/___\ \ \\\n";
        $logo .= "  /_________\ \ \\\n";
        $logo .= "  \____________\/\n";
        $text  = "     IllUSION\n";

        $style  = "\033[1m\033[37m%s\033[0m\033[0m";
        $style .= "\033[1m\033[36m%s\033[0m\033[0m";

        return sprintf($style, $logo, $text);
    }

    function tipex($arg) {

        $args = func_num_args() == 1 ? is_array($arg) ? $arg : [ $arg ] : func_get_args();

        foreach (array_reverse($args) as $arg) {

            echo "\x1B[1A" . str_repeat(' ', strlen(preg_replace("/\033\[\d+m/", '', $arg))) . "\r";
        }
    }

    function readLine() {

        return defined('STDIN') && !feof(STDIN) ? substr(fgets(STDIN), 0, -1) : null;
    }

    function sayByeOnExit() {

        defined('SIGINT') && pcntl_signal(SIGINT, function () { die("\rBye!\n"); }, 0);
    }

    function getString($text, $default = null, $validate = null) {

        $default && $text .= " \033[35m[default $default]\033[0m";
        $text .= " \033[33m(double tap ^C to abort)\033[0m";

        for ($i = 0; !$i || $error; $i++) {

            echo (@$error ?: $text) . "\n";
            $string = $this->readLine();

            $this->tipex($string);
            $i && $this->tipex($error);

            $string || $string = $default;
            $error = $validate ? $validate($string, "$text\n") : null;
            $error && $error = "\033[31m$error\033[0m";
        }

        $this->tipex($text);

        return $string;
    }

    function box($lines) {

        $rm = function ($s) { return preg_replace("/\033\[\d+m/", '', $s); };
        foreach ($lines as $line) $max = max(@$max, strlen($rm($line)));
        $box = $end = " \033[37m" . str_repeat('-', $max + 2) . "\033[0m\n";

        foreach ($lines as $line) {

            $box .= "\033[37m|\033[0m " . $line . str_repeat(' ', $max - strlen($rm($line))) . " \033[37m|\033[0m\n";
        }

        return $box . $end;
    }
}

class Statik {

//----------------------------------------------//
    static $COMPOSER =
//----------------------------------------------//

<<<_
{
    "name": "%s/%s",
    "description": "%s %s",
    "keywords": ["%s", "%s"],
    "license": "MIT",
    "require": {
        "php": ">=5.4.0",
        "hallemaen/illusion": "*"
    },
    "autoload": {
        "psr-4": {
            "%s\\\\": "src"
        }
    }
}
_;

//----------------------------------------------//
    static $README =
//----------------------------------------------//

<<<_
# %s

## Install
illusion add %s
_;

//----------------------------------------------//
    static $MAIN =
//----------------------------------------------//

<<<_
<?php

namespace %s;

/**
 * # alias %s
 * # context cli
 */
class Main {

    /**
     * # access %s
     * # alias main
     */
    static function _main(\$args, \$in, \$out, \$ctx) {

        \$out->end((new self)->main(@\$args['name']));
    }

    function main(\$name) {

        return "Hello \$name!";
    }
}
_;

}
