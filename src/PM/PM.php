<?php

namespace Illusion\Common\PM;

use Symfony\Component\Console\Input\ArrayInput;

use Symfony\Component\Console\Output\ConsoleOutput;

use GitScan\Application as GitScanApp;

use Illusion\Common\PC\PC;

use Illusion\Core\Launch;

use Illusion\Core\Util;

use Illusion\Config;

use Illusion\Core\LaunchException;

use Exception;

/**
 * # alias pm
 * # access pm
 * # context cli
 */
class PM {

    /**
     * # direct new
     */
    static function _new($args, $in, $out, $ctx) {

        (new Setup($ctx))->setupProject();
    }

    /**
     * # direct run
     */
    static function run($args, $in, $out, $ctx) {

        $action = t($args, '0/text');
        $raw = $in->vars()->raw();
        $raw = $action == $raw ? '' : substr($raw, strlen($action) + 1);

        if (!$action) {

            throw new Exception("no such action");
        }

        $pc = new PC();
        $list = $pc->read();
        $index = -1;

        foreach ($list as $row) {

            if ($row['action'] == $action || Util::startsWith($row['action'], "$action ") || Util::startsWith($row['action'], "$action:")) {

                $index = $row['index'];
                break;
            }
        }

        if ($index < 0 && !Launch::instance()->exists(preg_replace('/^[a-zA-Z0-9_-]+:/', '', $action))) {

            throw new Exception("no such action");
        }

        try {

            l($action . '.before', $args, $ctx);

        } catch (LaunchException $e) {

        }

        if (-1 < $index && $list[$index]['alive']) {

            $out->end('<red>already running</red>');
        }

        if (-1 < $index) {

            if (!$raw) {

                $action = $list[$index]['action'];
            }

            $pc->remove($index);
        }

        $result = $pc->add("$action $raw");
        $index = count($result) -1;
        $pc->aid($index, true);

        $out->raw('launching... ');

        $pc->fork($index);

        sleep(1);

        if ($pc->read()[$index]['alive']) {

            $out->put('<green>ok!</green>');

        } else {

            $out->put('<red>fail</red>');
        }

        sleep(2);

        $snap = $pc->snap($index);

        $out->put('');
        $out->put($snap);
    }

    /**
     * # direct end
     */
    static function end($args, $in, $out, $ctx) {

        $action = t($args, '0/text');

        if (!$action) {

            throw new Exception("bad action");
        }

        $pc = new PC();
        $list = $pc->read();
        $index = -1;

        foreach ($list as $row) {

            if ($row['action'] == $action || Util::startsWith($row['action'], "$action ") || Util::startsWith($row['action'], "$action:")) {

                $index = $row['index'];
                break;
            }
        }

        if ($index < 0) {

            throw new Exception("$action never run");
        }

        if (!$list[$index]['alive']) {

            $out->put('<red>not running</red>');

        } else {

            $out->raw('stopping... ');

            $pc->stop($index);

            sleep(1);

            if (!$pc->read()[$index]['alive']) {

                $out->put('<green>ok!</green>');

            } else {

                $out->put('<red>fail</red>');
            }
        }
    }

    static function uninstallProject($args, $in, $out, $ctx) {
        // this is needed.
        // maybe del should be named end and this named del
    }

    /**
     * # direct del
     */
    static function del($args, $in, $out, $ctx) {

        $action = t($args, '0/text');

        if (!$action) {

            throw new Exception("bad action");
        }

        self::end($args, $in, $out, $ctx);

        $pc = new PC();
        $list = $pc->read();
        $index = -1;

        foreach ($list as $row) {

            if ($row['action'] == $action || Util::startsWith($row['action'], "$action ") || Util::startsWith($row['action'], "$action:")) {

                $index = $row['index'];
                break;
            }
        }

        if ($index < 0) {

            throw new Exception("$action not found");
        }

        $out->raw('removing... ');

        $pc->remove($index);

        $out->put('<green>ok!</green>');
    }

    /**
     * # direct app
     */
    static function app($args, $in, $out, $ctx) {

        $app = t($args, '0/text');

        if (!$app) {

            throw new Exception('bad app');
        }

        $rep = null;

        foreach (Config::$SCAN as $ns => $path) {

            $rep = null;

            while (!Util::endsWith($path, '/vendor')) {

                $json = (array) @json_decode(@file_get_contents($path . '/illusion.json'));

                $apps = (array) @$json['apps'];

                if (@$apps[$app]) {

                    $rep = $apps[$app];

                    break;
                }

                $path = rtrim(preg_replace('/[^\/]*$/', '', $path), '/');
            }

            if ($rep) {

                break;
            }
        }

        if (!$rep) {

            throw new Exception('could not find app repository');
        }

        self::add([ $rep ], $in, $out, $ctx);
    }

    /**
     * # direct get
     */
    static function get($args, $in, $out, $ctx) {

    }

    /**
     * # direct add
     */
    static function add($args, $in, $out, $ctx) {

        $file = 'composer.json';
        $rep = t($args, '0/text');

        if (!$rep) {

            throw new Exception('bad repository');
        }

        if (!file_exists(Config::$DOCROOT . '/composer.json')) {

            throw new Exception('no local composer.json');
        }

        if (!file_exists(Config::$DOCROOT . '/illusion.json')) {

            throw new Exception('no local illusion.json');
        }

        $output = [];

        exec("git archive --format=zip --remote=$rep HEAD $file 2>&1", $output, $error);

        if ($error) {

            throw new Exception('no remote composer.json');
        }

        $data = implode("\n", $output);

        $head = "Vsig/vver/vflg/vmth/vmdt/vmdd/Vcrc/Vcsz/Vsze/vnml/vexl";
        $meta = unpack($head, substr($data, 0, 30));
        $name = substr($data, 30, $meta['nml']);
        $data = substr($data, 30 + $meta['nml'] + $meta['exl'], $meta['csz']);

        try {

            $meta['mth'] && $data = gzinflate($data);

        } catch (Exception $e) {

            throw new Exception('bad inflate');
        }

        $json = json_decode($data);

        if ($json === null) {

            throw new Exception('bad remote composer.json');
        }

        $find = trim(@key(@$json->autoload->{'psr-4'}), '\\');
        $last = trim(@current(@$json->autoload->{'psr-4'}), '\\');

        $name = @$json->name;
        $path = 'vendor/' . $name;
        $last && $path .= '/' . $last;

        if (!$find || !$name || !$path) {

            throw new Exception('not enough info in remote composer.json');
        }

        $illusion = json_decode($illbk = file_get_contents(Config::$DOCROOT . '/illusion.json'));
        $composer = json_decode($combk = file_get_contents(Config::$DOCROOT . '/composer.json'));

        if ($illusion === null) {

            throw new Exception('bad local illusion.json');
        }

        if ($composer === null) {

            throw new Exception('bad local composer.json');
        }

        if (!@$illusion->locations) {

            $illusion->locations = (object) [];
        }

        if (!@$composer->require) {

            $composer->require = (object) [];
        }

        if (!@$composer->repositories) {

            $composer->repositories = [];
        }

        if (@$composer->require->{$name}) {

            throw new Exception('already there');
        }

        $illusion->locations->{$find} = $path;
        $composer->require->{$name} = 'dev-master';
        $composer->repositories[] = (object) [ 'type' => 'vcs', 'url' => $rep ];

        file_put_contents(Config::$DOCROOT . '/illusion.json', json_encode($illusion, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        file_put_contents(Config::$DOCROOT . '/composer.json', json_encode($composer, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        passthru('( cd ' . Config::$DOCROOT . ' ; php composer.phar update ' . $name . ' )', $error);

        if ($error) {

            file_put_contents(Config::$DOCROOT . '/illusion.json', $illbk);
            file_put_contents(Config::$DOCROOT . '/composer.json', $combk);
        }
    }

    /**
     * # direct status
     */
    static function _status($args, $in, $out, $ctx) {

        $array = [ 'path' => [ Config::$INITDIR ] ];

        (new GitScanApp())->get('status')->run(new ArrayInput($array), new ConsoleOutput());
    }

    /**
     * # direct install
     * # direct self
     */
    static function pm($args, $in, $out, $ctx) {

        $package = t($args, '0/text');
        $packages = array_diff(get_class_methods(__CLASS__), [ 'pm' ]);

        if ($package && in_array($package, $packages)) {

            l('pm.' . $package, $args, $ctx);

        } else {

            throw new Exception('bad package');
        }
    }
    static function status($args, $in, $out, $ctx) {

        $array = [ 'path' => [ Config::$DOCROOT ] ];

        (new GitScanApp())->get('status')->run(new ArrayInput($array), new ConsoleOutput());
    }

    static function deploy($args, $in, $out, $ctx) {

        global $argv;
        $copy = $argv;

        (new Deploy(Config::$DOCROOT, array_slice($copy, 3)))->begin();
    }

    static function update($args, $in, $out, $ctx) {

        (new Update(Config::$DOCROOT))->begin();
    }

    static function install($args, $in, $out, $ctx) {

        (new Install(Config::$DOCROOT))->install();
    }

    static function uninstall($args, $in, $out, $ctx) {

        (new Install(Config::$DOCROOT))->uninstall();
    }

    static function frontend($args, $in, $out, $ctx) {

        $version = `nginx -v 2>&1`;

        if (!Util::startsWith($version, 'nginx')) {

            $_[] = '<bold>nginx not installed</bold>';
            $_[] = '';
            $_[] = 'apt-get install nginx';

            $out->put($out->tty()->box($_));

            return;
        }

        $out->end('ok!');
    }
}
