<?php

namespace Illusion\Common\PC;

use Illusion\Core\DataStore;

use Illusion\Core\Util;

use Illusion\Config;

use Exception;

class PCCMD {

    protected $in;
    protected $out;

    protected $procCtrl;
    protected $dataStore;

    function __construct($in, $out) {

        $this->in = $in;
        $this->out = $out;

        $this->pc = new PC();
        $this->ds = DataStore::instance();
    }

    function illBeBack() {

        pcntl_signal(SIGINT, function () { exit("\rI'll be back.\n"); }, 0);
    }

    function getInteger($label, $integer = null) {

        $integer = t($integer, 'natural?digit', -1);

        for ($i = 0; $integer < 0; $i++) {

            $upper = strtoupper($label);

            $text = "Choose $label (double tap ^C to abort)";
            $text = $i ? $i % 3 ? "... which $label, sir?" : "FFS WHICH #$@&% $upper!?" : $text;

            $integer = t($this->in->more($text), 'natural?digit', -1);
        }

        return $integer;
    }

    function getString($label, $string = null) {

        $string = t($string, 'line');

        for ($i = 0; !$string; $i++) {

            $upper = strtoupper($label);

            $text = "Choose $label (double tap ^C to abort)";
            $text = $i ? $i % 3 ? "... which $label, sir?" : "FFS WHICH #$@&% $upper!?" : $text;

            $string = t($this->in->more($text), 'line');
        }

        return $string;
    }

    function getBoolean($text, $line = null, $default = null) {

        $default || $default = 'y';

        $line = t($line, 'text');

        if (!$line) {

            $text .= ' ' . ($default == 'y' ? '(YES|no)' : '(yes|NO)');
            $line = strtolower($this->in->more($text));
        }

        return $default == 'y' ? $line != 'n' && $line != 'no' : $line == 'y' || $line == 'yes';
    }

    function add($action) {

        $this->illBeBack();

        $action = $this->getString('action', $action);

        $list = $this->pc->add($action);

        $this->out->put('<green>#' . (count($list) - 1) .  " $action added.</green>");
    }

    function remove($index, $force = null) {

        $this->illBeBack();

        $index = $this->getInteger('index', $index);

        $list = $this->ds->load('common.pc.list') ?: [];

        if (isset($list[$index]['action'])) {

            if (!$this->getBoolean("Removing item with action {$list[$index]['action']}. Continue?", $force)) {

                $this->out->end('<red>Aborted.</red>');
            }

        } else {

            $this->out->end("<red>It's not very effective...</red>");
        }

        $this->pc->remove($index);

        $this->out->end("<green>It's Super Effective!</green>");
    }

    function clear($force = null) {

        $this->illBeBack();

        if (!$this->getBoolean('This is a destructive action. Continue?', $force)) {

            $this->out->end('<red>Aborted.</red>');

        } else {

            $this->pc->clear();

            $this->out->end('<green>List cleared.</green> We hope to see you again!');
        }
    }

    function dump() {

        $json = Util::file($this->ds->resolve('common.pc.list'));

        $json = Util::prettyPrintJSON($json);

        $this->out->end($json);
    }

    function resolve($index) {

        $index = t($index, 'natural?digit', -1);

        if (-1 < $index) {

            $list = $this->ds->load('common.pc.list') ?: [];

            if (!isset($list[$index])) {

                $this->out->put('<red>Bad index</red>');
            }

            $this->out->put(Config::$_WPATH_DATA . '/' . md5('common.pc') . '/' . $list[$index]['hash']);

        } else {

            $this->out->put($this->ds->resolve('common.pc.list'));
        }
    }

    function show() { //??

        $items = $this->pc->getActiveItems();

        $list = $this->ds->load('common.pc.list') ?: [];

        foreach ($list as $n => $item) {

            $list[$n]['alive'] = isset($items[$item['hash']]);

            unset($list[$n]['hash']);
        }

        $this->out->put($list ?: 'Nothing to see here, move along...');
    }

    function aid($index, $aid = null) {

        $this->illBeBack();

        $aid = $this->getBoolean('Turn <green>ON</green> process aid?', $aid);
        $index = $this->getInteger('index', $index);

        try {

            $this->pc->aid($index, $aid);

            $this->out->end("<green>It's Super Effective!</green>");

        } catch (Exception $e) {

            $this->out->end("<red>It's not very effective...</red>");
        }
    }

    function fork($index, $force = null) {

        $this->illBeBack();

        $index = $this->getInteger('index', $index);

        $list = $this->ds->load('common.pc.list') ?: [];

        if (!isset($list[$index])) {

            throw new Exception('bad index');
        }

        $items = $this->pc->getActiveItems();

        if (isset($items[$list[$index]['hash']])) {

            if (!$this->getBoolean('It exists, are u sure?', $force, 'n')) {

                $this->out->end('<red>Aborted.</red>');
            }
        }

        try {

            $error = $this->pc->fork($index, true);

            $this->out->put("<cyan>Commencing countdown... 3 2 1 FORK!</cyan>");

            if ($error) {

                $this->out->put("<red>$error</red>");
            }

        } catch (Exception $e) {

            $this->out->put('<red>' . t($e->getMessage(), 'text-c') . '</red>'); //?? test is already alive
        }
    }

    function tail($index) {

        $this->illBeBack();

        $index = $this->getInteger('index', $index);

        $list = $this->ds->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            $this->out->put('<red>Bad index</red>');

            return;
        }

        $this->out->put('');

        $this->out->put("<cyan>tailing #$index {$list[$index]['action']}</cyan> (tap ^C to abort)");

        $this->out->put('');

        try {

            $this->pc->tail($index);

        } catch (Exception $e) {

            $this->out->put('<red>' . t($e->getMessage(), 'text-c') . '</red>');
        }
    }

    function snap($index) { //?? test

        $index = $this->getInteger('index', $index);

        $list = $this->ds->load('common.pc.list') ?: [];

        $this->out->put('');

        $this->out->put("<cyan>snapshot of #$index {$list[$index]['action']}</cyan>");

        $this->out->put('');

        try {

            $lines = $this->pc->snap($index);

            $this->out->put($lines);

        } catch (Exception $e) {

            $this->out->put('<red>' . t($e->getMessage(), 'text-c') . '</red>');
        }
    }

    function kill($index) {

        $this->illBeBack();

        $index = $this->getInteger('index', $index);

        try {

            $this->pc->kill($index);

            $this->out->end("<green>It's Super Effective!</green>");

        } catch (Exception $e) {

            $this->out->end("<red>It's not very effective...</red>");
        }
    }

    function stop($index) {

        $this->illBeBack();

        $index = $this->getInteger('index', $index);

        try {

            $this->pc->stop($index);

            $this->out->end("<green>It's Super Effective!</green>");

        } catch (Exception $e) {

            $this->out->end("<red>It's not very effective...</red>");
        }
    }

    function poke($index) {

        $this->illBeBack();

        $index = $this->getInteger('index', $index);

        try {

            $this->pc->poke($index);

            $this->out->end("<green>It's Super Effective!</green>");

        } catch (Exception $e) {

            $this->out->end("<red>It's not very effective...</red>");
        }
    }

    function abandon($index) {

        $this->illBeBack();

        $index = $this->getInteger('index', $index);

        try {

            $this->pc->abandon($index);

            $this->out->end("<green>It's Super Effective!</green>");

        } catch (Exception $e) {

            $this->out->end("<red>It's not very effective...</red>");
        }
    }
}
