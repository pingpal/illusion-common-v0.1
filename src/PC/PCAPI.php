<?php

namespace Illusion\Common\PC;

use Exception;

/**
 * # alias pc
 * # access pc
 * # context api
 * # allow super
 *
 * # route post|put:/pc		api	pc.add
 * # route get:/pc			api	pc.get
 * # route delete:/pc/(\d+)	api	pc.remove index
 */

class PCAPI {

    static function add($args, $in, $out) {

        (new PC())->add(@$args['action']);
    }

    static function remove($args, $in, $out) {

        try {

            (new PC())->remove(@$args['index']);

        } catch (Exception $e) {

            $out->setStatusCode(400);

            throw $e;
        }
    }

    static function clear($args, $in, $out) {

        (new PC())->clear();
    }

    static function read($args, $in, $out) {

        $out->end((new PC())->read(@$args['index']));
    }

    static function aid($args, $in, $out) {

        (new PC())->aid(@$args['index'], @$args['aid'] === 'yes');
    }

    static function fork($args, $in, $out) {

        (new PC())->fork(@$args['index']);
    }

    static function snap($args, $in, $out) {

        $out->end((new PC())->snap(@$args['index']));
    }

    static function kill($args, $in, $out) {

        (new PC())->kill(@$args['index']);
    }

    static function stop($args, $in, $out) {

        (new PC())->stop(@$args['index']);
    }

    static function poke($args, $in, $out) {

        (new PC())->poke(@$args['index']);
    }

    static function abandon($args, $in, $out) {

        (new PC())->abandon(@$args['index']);
    }
}
