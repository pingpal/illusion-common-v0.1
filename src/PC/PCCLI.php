<?php

namespace Illusion\Common\PC;

use Illusion\Core\LaunchHelp;

use Illusion\Core\Util;

/**
 * # alias pc
 * # access pc
 * # context cli
 */
class PCCLI {

    /**
     * # direct pc
     */
    static function pc($args, $in, $out, $ctx) {

        $lines[] = '<bold>CLI Process Control</bold>';
        $lines[] = '';
        $lines[] = 'Available commands:';
        $lines[] = '';

        foreach ((new LaunchHelp($ctx->id()))->getInfo()['alias'] as $alias => $method)
        if (Util::startsWith($alias, 'pc.') && Util::startsWith($method, get_class()))
        $aliases[] = ['cmd' => $alias, 'method' => $method ];
        $aliases = $out->tty()->format(@$aliases);
        $lines = array_merge($lines, explode("\n", $aliases));

        $out->end($out->tty()->box($lines));
    }

    static function add($args, $in, $out) {

        (new PCCMD($in, $out))->add(@$args['action']);
    }

    /**
     * # alias rm
     */
    static function remove($args, $in, $out) {

        (new PCCMD($in, $out))->remove(@$args['index'], @$args['force']);
    }

    static function clear($args, $in, $out) {

        (new PCCMD($in, $out))->clear(@$args['force']);
    }

    static function dump() {

        (new PCCMD($in, $out))->dump();
    }

    static function resolve($args, $in, $out) {

        (new PCCMD($in, $out))->resolve(@$args['index']);
    }

    /**
     * # alias list
     * # alias ls
     */
    static function show($args, $in, $out) {

        (new PCCMD($in, $out))->show();
    }

    static function aid($args, $in, $out) {

        (new PCCMD($in, $out))->aid(@$args['index'], @$args['aid']);
    }

    static function fork($args, $in, $out) {

        (new PCCMD($in, $out))->fork(@$args['index'], @$args['force']);
    }

    static function test($args, $in, $out) {

        (new PCCMD($in, $out))->test();
    }

    static function tail($args, $in, $out) {

        (new PCCMD($in, $out))->tail(@$args['index']);
    }

    static function snap($args, $in, $out) {

        (new PCCMD($in, $out))->snap(@$args['index']);
    }

    static function kill($args, $in, $out) {

        (new PCCMD($in, $out))->kill(@$args['index']);
    }

    static function stop($args, $in, $out) {

        (new PCCMD($in, $out))->stop(@$args['index']);
    }

    static function poke($args, $in, $out) {

        (new PCCMD($in, $out))->poke(@$args['index']);
    }

    static function abandon($args, $in, $out) {

        (new PCCMD($in, $out))->abandon(@$args['index']);
    }

    static function time($args, $in, $out) {

        (new PCCMD($in, $out))->time();
    }
}
