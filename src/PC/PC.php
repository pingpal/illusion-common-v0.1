<?php

namespace Illusion\Common\PC;

use Illusion\Core\OptParse;

use Illusion\Core\DataStore;

use Illusion\Core\Util;

use Illusion\Config;

use Exception;

class PC {

    protected $dataStore;

    function __construct() {

        $this->dataStore = DataStore::instance();
    }

    function getLastLines($filepath, $lines = 1, $adaptive = true) {

        // Open file
        $f = @fopen($filepath, 'rb');

        if ($f === false) {

            return '';
        }

        // Sets buffer size
        if (!$adaptive) {

            $buffer = 4096;

        } else {

            $buffer = ($lines < 2 ? 64 : ($lines < 10 ? 512 : 4096));
        }

        // Jump to last character
        fseek($f, -1, SEEK_END);

        // Read it and adjust line number if necessary
        // (Otherwise the result would be wrong if file doesn't end with a blank line)
        if (fread($f, 1) != "\n") $lines -= 1;

        // Start reading
        $output = '';
        $chunk = '';

        // While we would like more
        while (ftell($f) > 0 && $lines >= 0) {

            // Figure out how far back we should jump
            $seek = min(ftell($f), $buffer);

            // Do the jump (backwards, relative to where we are)
            fseek($f, -$seek, SEEK_CUR);

            // Read a chunk and prepend it to our output
            $output = ($chunk = fread($f, $seek)) . $output;

            // Jump back to where we started reading
            fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);

            // Decrease our line counter
            $lines -= substr_count($chunk, "\n");

        }

        // While we have too many lines
        // (Because of buffer size we might have read too many)
        while ($lines++ < 0) {

            // Find first newline and remove all text before that
            $output = substr($output, strpos($output, "\n") + 1);
        }

        // Close file and return
        fclose($f);

        return trim($output);
    }

    function psAuxGrep($cmd) {

        exec("ps axo pid,ppid,args | grep '$cmd' | grep -v grep", $lines);

        return $lines;
    }

    function getActiveItems() {

        $cmd = Config::$PATH_EXE . ' ' . Config::$_PATH_DETACH . '.*' . md5('common.pc');

        $items = [];

        foreach ($this->psAuxGrep($cmd) as $line) {

            if (preg_match("#(\d+)\s+(\d+)\s+$cmd/(\w+)#", $line, $hits)) {

                if (!isset($items[$hits[3]]) || $items[$hits[3]] === $hits[2]) {

                    $items[$hits[3]] = $hits[1];
                }
            }
        }

        return $items;
    }

    function getActiveAngels() {

        $cmd = Config::$PATH_EXE . ' ' . Config::$_PATH_DETACH . '.*' . md5('common.pc');

        $items = [];

        foreach ($this->psAuxGrep($cmd) as $line) {

            if (preg_match("#(\d+)\s+(\d+)\s+$cmd/(\w+)#", $line, $hits)) {

                if ($hits[2] !== '1') {

                    $items[$hits[3]] = $hits[2];
                }
            }
        }

        return $items;
    }

    function add($action) {

        $action = t($action, 'line');

        if (!$action) {

            throw new Exception('bad action');
        }

        $list = $this->dataStore->load('common.pc.list') ?: [];

        $list[] = ['#' => count($list), 'action' => $action, 'aid' => false, 'hash' => md5(microtime())];

        $this->dataStore->save('common.pc.list', $list);

        return $list;
    }

    function remove($index) {

        $index = t($index, 'natural?digit', -1);

        $list = $this->dataStore->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            throw new Exception('bad index');
        }

        $amended = [];

        foreach ($list as $row) {

            if ($row['#'] !== $index) {

                $row['#'] = count($amended);
                $amended[] = $row;
            }
        }

        $this->dataStore->save('common.pc.list', $amended);

        return $amended;
    }

    function clear() {

        $this->dataStore->save('common.pc.list', []);
    }

    function read() {

        $items = $this->getActiveItems();

        $list = $this->dataStore->load('common.pc.list') ?: [];

        foreach ($list as $n => $item) {

            $list[$n]['alive'] = isset($items[$item['hash']]);
            $list[$n]['index'] = $list[$n]['#'];

            unset($list[$n]['hash'], $list[$n]['#']);
        }

        return $list;
    }

    function aid($index, $aid) {

        $index = t($index, 'natural?digit', -1);

        if ($index < 0) {

            throw new Exception('bad index');
        }

        $list = $this->dataStore->load('common.pc.list') ?: [];

        $isset = isset($list[$index]['aid']) && $list[$index]['aid'] === !$aid;

        if ($isset) {

            $list[$index]['aid'] = !!$aid;

            $this->dataStore->save('common.pc.list', $list);

        } else {

            throw new Exception('invalid arg');
        }
    }

    function fork($index, $force = false) {

        $index = t($index, 'natural?digit', -1);

        $list = $this->dataStore->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            throw new Exception('bad index');
        }

        $items = $this->getActiveItems();

        if (!$force && isset($items[$list[$index]['hash']])) {

            throw new Exception('already alive');
        }

        $action = $list[$index]['action'];
        $action = preg_replace('/^[a-zA-Z0-9_-]+:/', '', $action);
        $args = OptParse::instance()->apply($action);
        $action = explode(' ', $action)[0];

        $path = Config::$_WPATH_DATA . '/' . md5('common.pc');

        Util::isDirWritable($path) || Util::mkdir($path);

        $log = Config::$_WPATH_DATA . '/' . md5('common.pc') . '/' . $list[$index]['hash'];

        Util::isFile($log) || @fclose(Util::open($log));

        return d($action, $args, "$path/{$list[$index]['hash']}", $list[$index]['aid']);
    }

    function tail($index) {

        $index = t($index, 'natural?digit', -1);

        $list = $this->dataStore->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            throw new Exception('bad index');
        }

        $path = Config::$_WPATH_DATA . '/' . md5('common.pc') . '/' . $list[$index]['hash'];

        Util::isFile($path) || @fclose(Util::open($path));

        passthru("tail -f $path");
    }

    function snap($index) {

        $index = t($index, 'natural?digit', -1);

        $list = $this->dataStore->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            throw new Exception('bad index');
        }

        $path = Config::$_WPATH_DATA . '/' . md5('common.pc') . '/' . $list[$index]['hash'];

        Util::isFile($path) || @fclose(Util::open($path));

        return $this->getLastLines($path, 20);
    }

    function kill($index) {

        $index = t($index, 'natural?digit', -1);

        $list = $this->dataStore->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            throw new Exception('bad index');
        }

        $items = $this->getActiveItems();

        $killed = isset($items[$list[$index]['hash']]) && posix_kill($items[$list[$index]['hash']], 9 /*SIGKILL*/);

        if (!$killed) {

            throw new Exception('process not found');
        }
    }

    function stop($index) {

        $index = t($index, 'natural?digit', -1);

        $list = $this->dataStore->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            throw new Exception('bad index');
        }

        $items = $this->getActiveItems();

        $killed = isset($items[$list[$index]['hash']]) && posix_kill($items[$list[$index]['hash']], 15 /*SIGTERM*/); //??

        if (!$killed) {

            throw new Exception('process not found');
        }
    }

    function poke($index) {

        $index = t($index, 'natural?digit', -1);

        $list = $this->dataStore->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            throw new Exception('bad index');
        }

        $items = $this->getActiveItems();

        $killed = isset($items[$list[$index]['hash']]) && posix_kill($items[$list[$index]['hash']], 16 /*SIGUSR1*/); //??

        if (!$killed) {

            throw new Exception('process not found');
        }
    }

    function abandon($index) {

        $index = t($index, 'natural?digit', -1);

        $list = $this->dataStore->load('common.pc.list') ?: [];

        if ($index < 0 || !isset($list[$index])) {

            throw new Exception('bad index');
        }

        $items = $this->getActiveAngels();

        $killed = isset($items[$list[$index]['hash']]) && posix_kill($items[$list[$index]['hash']], 9 /*SIGKILL*/);

        if (!$killed) {

            throw new Exception('process not found');
        }
    }
}
