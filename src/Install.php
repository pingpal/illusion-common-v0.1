<?php

namespace Illusion\Common;

use Illusion\Access\Sql\User;

use Illusion\Core\ConfigEdit;

use Illusion\Core\Router;

use Illusion\Config;

use Exception;

class Install {

    /**
     * # access webif
     * # context api
     * # direct install
     */
    static function main($args, $in, $out, $ctx) {

        if (!Config::$INSTALL) {

            throw new Exception('installed');
        }

        $ce = ConfigEdit::instance();
        $ec = new EnvCheck($ctx, $ce);
        $router = new Router($in->vars());

        $router->on('/api/install/start', function () use ($args, $in, $out, $ctx, $ec, $ce) {

            $out->end('ok');
        });

        $router->on('/api/install/requirements', function () use ($args, $in, $out, $ctx, $ec, $ce) {

            $ce->setRuntimeConfig('Illusion\Config', $args);

            $out->end($ec->check([ 'pathinfo', 'reachable', 'database' ], true));
        });

        $router->on('/api/install/config/save', function () use ($args, $in, $out, $ctx, $ec, $ce) {

            $ce->setRuntimeConfig('Illusion\Config', $args);
            $cf = $ce->setConfig('Illusion\Writable\Custom', $args);
            $ce->stash('Illusion\Writable\Custom', $cf);

            $out->end('ok');
        });

        $router->on('/api/install/config', function () use ($args, $in, $out, $ctx, $ec, $ce) {

            $out->end($ce->getDefaultConfig('Illusion\Config', [ 'install', 'writable', 'environments', 'command' ], true));
        });

        $router->on('/api/install/verify', function () use ($args, $in, $out, $ctx, $ec, $ce) {

            $out->end($ec->check([ 'executable', 'database' ]));
        });

        $router->on('/api/install/finish', function () use ($args, $in, $out, $ctx, $ec, $ce) {

            $that = new Main();

            foreach ($that->tables() as $table) {

                sql()->createTableFromName($table);
            }

            $username = t($args, 'username/text-l');
            $password = t($args, 'password/text-m');

            $user = (new User())->add();

            sql()->insert()->into('group')->set('user', $user)->set('name', 'super')->exe();

            $sql = sql()->insert()->into('mail');

            $sql->set('ok', true);
            $sql->set('user', $user);
            $sql->set('mail', $username);
            $sql->set('hash', password_hash($password, PASSWORD_DEFAULT));

            $sql->exe();

            $config = [ 'INSTALL' => false ] + $ce->load('Illusion\Writable\Custom');
            $ce->setRuntimeConfig('Illusion\Config', $config, true);
            $ce->save('Illusion\Writable\Custom', $config);

            $out->end('ok');
        });

        $out->headersSent() || $out->failure('bad request');
    }
}
